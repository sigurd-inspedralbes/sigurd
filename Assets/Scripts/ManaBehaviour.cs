using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ManaBehaviour : MonoBehaviour
{
    [SerializeField] float mana, maxmana;
    [SerializeField] bool RefillGradually;
    [SerializeField] float RefillSpeed;
    public UnityEvent<float, float> OnAlterMana;


    void Refill(){this.mana = this.maxmana; OnAlterMana.Invoke(this.mana, this.maxmana); }
    
    void ConsumeMana(float manatoquit) { this.mana -= manatoquit; OnAlterMana.Invoke(this.mana, this.maxmana); }    
    
    void AddMana(float amounttoadd) {this.mana += amounttoadd; OnAlterMana.Invoke(this.mana, this.maxmana);}

    bool CanUseMana(float manatoquit)
    {
        if ((mana - manatoquit) >= 0)
            return true;
        else
            return false;
    }
    
    private void Update()
    {
        if (RefillGradually && mana < maxmana)
        {
            mana += RefillSpeed * Time.deltaTime;
            if (mana > maxmana)
                Refill();
            OnAlterMana.Invoke(this.mana, this.maxmana);
        }
    }
}
