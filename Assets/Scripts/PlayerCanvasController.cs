using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PlayerCanvasController : MonoBehaviour
{
    CanvasGroup cg;
    bool blockCanvas;
    public float timeToStartHideCanvas, canvasHideSpeed;
    [SerializeField]float timer;
    private void Awake()
    {
        cg = this.GetComponent<CanvasGroup>(); 
        timer = timeToStartHideCanvas;
    }
    public void reverse()
    {
        blockCanvas = blockCanvas ? false : true;
        SetCanvasVisible(blockCanvas);
    }
    public void SetCanvasVisible(bool newvalue)
    {
        blockCanvas = newvalue;
        if (blockCanvas)
        {   
            timer = timeToStartHideCanvas;
            cg.alpha = 1;
        }
        else
            cg.alpha = 0;
    }
    public void HideCanvas()
    {
        
    }
    public void OnEnable()
    {
        cg.alpha = 1;
        timer = timeToStartHideCanvas;
    }
    private void Update()
    {
        if (!blockCanvas)
        {
            if (timer >= 0)
                timer -= Time.deltaTime;
            else
                if (cg.alpha > 0)
                cg.alpha -= canvasHideSpeed * Time.deltaTime;

        }
    }
}
