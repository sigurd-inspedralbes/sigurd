using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    public int value;
    public float velocityRotation;

    private void OnTriggerEnter(Collider other)
    {
        LevelManager.instance.IncrementCoin(value);
        Destroy(gameObject);
    }
    private void Update()
    {
        transform.Rotate(Vector3.forward * velocityRotation * Time.deltaTime);
    }
}
