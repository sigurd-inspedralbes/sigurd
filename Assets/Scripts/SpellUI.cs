using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SpellUI : MonoBehaviour
{
    private ThrowSpell playerspellmanager;
    public Sprite NoSpellSprite;
    Image img;
    private void Start()
    {
        img = this.GetComponent<Image>();
        playerspellmanager = GameObject.Find("SigurdDef").GetComponent<ThrowSpell>();
        if (playerspellmanager.hechizos.Count == 0)
            img.sprite = NoSpellSprite;

    }
    public void ChangeUISpell()
    {
        if(playerspellmanager.hechizos.Count > 0)
            img.sprite = playerspellmanager.GetSpell().icon;
    }
}
