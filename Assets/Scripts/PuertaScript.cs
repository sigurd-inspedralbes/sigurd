using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuertaScript : MonoBehaviour
{
    private int dianas;
    public int tipoDeReto;
    private Animator anim;
    private void Start()
    {
 
        anim = GetComponent<Animator>();
    }
    public void AbrirPuerta()
    {
        anim.SetBool("AbrirPuerta", true);
    }
    public void CloseDoor()
    {
        anim.SetBool("AbrirPuerta", false);
    }
}
