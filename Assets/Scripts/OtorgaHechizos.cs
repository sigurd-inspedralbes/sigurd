using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
public class OtorgaHechizos : MonoBehaviour
{
    public Hechizo hechizo;
    public UnityEvent OnCollect;
    private void OnTriggerEnter(Collider other)
    {
        if(other.TryGetComponent<ThrowSpell>(out ThrowSpell tsp))
        {
            if (tsp.hechizos.Count == 0)
                GameObject.Find("SpellUI").GetComponent<Image>().sprite = hechizo.icon;
            tsp.hechizos.Add(hechizo);
            Destroy(this.gameObject);
            OnCollect.Invoke();
        }
    }
}
