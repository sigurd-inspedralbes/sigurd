using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class MecanismoDianas : MonoBehaviour
{
    public UnityEvent activate;

    public void CheckForChildCount()
    {
        if (this.transform.childCount <= 1)
            activate.Invoke();
    }
}
