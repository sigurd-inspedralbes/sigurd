using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderEffect : MonoBehaviour
{

    [Header("Bar Gradient Color(s)")]
    public Gradient gradient;

    [Header("Components")]
        [Tooltip("Image for the value")]
        public Image Image;
        [Tooltip("Image for the effect")]
        public Image EffectImage;

    [Header("Values")]
        [Tooltip("Numeros menores, m�s rapido, numeros mayores m�s lento")]
        public float delay;

    [SerializeField] private float quitSpeed = 0.005f;


    [Header("Debug")]
    [Tooltip("Variables para debug")]
    public bool debug;
    [SerializeField] float val, max;
    public void UpdateUI(float value, float maxvalue)
    {
        Image.color = gradient.Evaluate(1f);
        Image.fillAmount = value / maxvalue;
        Image.color = gradient.Evaluate(EffectImage.fillAmount);

        if (EffectImage.fillAmount > Image.fillAmount)
            EffectImage.fillAmount -= quitSpeed / delay;
        else
            EffectImage.fillAmount = Image.fillAmount;
    }

    private void Update()
    {
        if (debug)
            UpdateUI(val, max);
    }
}
