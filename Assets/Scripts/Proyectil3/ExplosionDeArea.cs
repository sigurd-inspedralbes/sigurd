using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionDeArea : MonoBehaviour
{
    public float timer = 2f;
    public int damage=20;
    private HealthBehaviour hbr;

    private void Start()
    {
        hbr = GetComponent<HealthBehaviour>();
    }
    private void OnTriggerEnter(Collider other)
    {
        
        if (other.TryGetComponent<HealthBehaviour>(out HealthBehaviour hbr))           
            hbr.TakeDamage(this.damage);
    }
    private void Update()
    {
        timer -= Time.deltaTime;
        if (timer <= 0)
            Destroy(gameObject);
    }
}
