using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProyectilBomba : MonoBehaviour
{
    public float fuerzaInicial = 5f; // Fuerza inicial del lanzamiento
    public float alturaMaxima = 2.5f; // Altura m�xima de la par�bol
    public float spellSpeed;
    private Rigidbody rgb;
    public GameObject explosion;

    void Start()
    {
        //damage = GetComponent<HealthBehabiour>;
        rgb = GetComponent<Rigidbody>();
        Lanzar();
    }


    void Lanzar()
    {
        // Calcular la velocidad inicial en funci�n de la altura m�xima y la gravedad
        float velocidadInicialY = Mathf.Sqrt(2f * fuerzaInicial * alturaMaxima * Mathf.Abs(Physics.gravity.y));

        // Lanzar la pelota en la direcci�n hacia adelante y hacia arriba
        Vector3 direccionLanzamiento = transform.forward + transform.up;
        rgb.velocity = direccionLanzamiento * fuerzaInicial;
        rgb.velocity += transform.up * velocidadInicialY;
    }
    private void OnCollisionEnter(Collision collision)
    {
        Instantiate(explosion, transform.position + new Vector3(0, 0, 0), transform.rotation);

        Destroy(gameObject);
    }
}
