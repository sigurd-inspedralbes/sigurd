using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PuertaFinal : MonoBehaviour
{
    public int CollectiblesToPass;
    public UnityEvent OnComplete;

    public void ItemCollected()
    {
        CollectiblesToPass--;
        if (CollectiblesToPass == 0)
            OnComplete.Invoke();
    }
}
