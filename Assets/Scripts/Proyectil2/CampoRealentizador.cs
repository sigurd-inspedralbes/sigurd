using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine;

public class CampoRealentizador : MonoBehaviour
{
    public float timer = 10f;
    private AISimples enemy;

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<AISimples>(out AISimples enemy))
            enemy.realentizado = true;
    }
    private void Update()
    {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            Destroy(gameObject);
            enemy.realentizado = false;
        }
    }
}
