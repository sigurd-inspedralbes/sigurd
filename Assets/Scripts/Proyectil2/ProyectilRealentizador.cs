using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProyectilRealentizador : MonoBehaviour
{
    public float spellSpeed;
    private Rigidbody rgb;
    public GameObject proyectilRealentizador; 

    void Start()
    {
        rgb = GetComponent<Rigidbody>();
        //damage = GetComponent<HealthBehabiour>;
    }

    void Update()
    {
        rgb.velocity = new Vector3(transform.forward.x, 0, transform.forward.z) * spellSpeed;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Amigos") || collision.gameObject.layer == 7)
            Instantiate(proyectilRealentizador, transform.position+new Vector3(0,-1,0), transform.rotation);
        
        Destroy(gameObject);
    }
}
