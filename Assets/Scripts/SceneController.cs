using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneController : MonoBehaviour
{
    [SerializeField] GameObject LoadingScreen;

    public void OpenScene(string SceneName)
    {
        SceneManager.LoadScene(SceneName);
    }
    public void OpenSceneWithLoadingScreen(string SceneName)
    {
        if (LoadingScreen)
        {
            LoadingScreen.SetActive(true);
            StartCoroutine(LoadNextLevel(SceneName));
        }
        else
            SceneManager.LoadScene(SceneName);
    }
    public void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    IEnumerator LoadNextLevel(string SceneName)
    {
        AsyncOperation loadLevel = SceneManager.LoadSceneAsync(SceneName);
        while (!loadLevel.isDone)
            yield return null;
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
