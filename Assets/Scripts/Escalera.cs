using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Escalera : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<playerAnimatorController>(out playerAnimatorController pac))
            pac.SetClimbingAnimation(true, this.transform.rotation);
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.TryGetComponent<playerAnimatorController>(out playerAnimatorController pac))
            pac.SetClimbingAnimation(false, this.transform.rotation);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.TryGetComponent<playerAnimatorController>(out playerAnimatorController pac))
            pac.SetClimbingAnimation(true, this.transform.rotation);
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.TryGetComponent<playerAnimatorController>(out playerAnimatorController pac))
            pac.SetClimbingAnimation(false, this.transform.rotation);
    }
}
