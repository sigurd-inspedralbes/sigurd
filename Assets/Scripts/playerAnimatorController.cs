using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class playerAnimatorController : MonoBehaviour
{
    public UnityEvent OnDieAnimationEvents;
    Animator animator;
    private StarterAssets.ThirdPersonController tps;

    void Start(){animator = this.GetComponent<Animator>();tps = this.GetComponent<StarterAssets.ThirdPersonController>(); }

    public void SetAllBoolFalse()
    {
        for (int i = 0; i < animator.parameterCount; i++)
            if (animator.GetParameter(i).type == AnimatorControllerParameterType.Bool)
                animator.SetBool(animator.GetParameter(i).name, false); 
    }

    public void SetClimbingAnimation(bool value, Quaternion ladderrotation) 
    { 
        animator.SetBool("Climbing", value);       
        this.transform.rotation = ladderrotation;
        tps.rotationlocked = value;
        if (!value)
            tps.hormovelock = value;
    }
    public void SetAttackAnimation(bool value) { animator.SetBool("Attack", value); }
    public void SetHasHittedAnimation(bool value) { animator.SetBool("Hashitted", value); }
    public void SetBlockAnimation(bool value) { animator.SetBool("Block", value); }
    public void SetSlideEnvainarAnimation(bool value) { animator.SetBool("SlideEnvainar", value); }
    
    public void SetSpellAnimation(bool value) 
    { 
        if(this.GetComponent<ThrowSpell>().hechizos.Count != 0)
            animator.SetBool("Spell", value); 
    }

    public void SetEnvainada(bool newstate) 
    {
        animator.SetBool("Envainada", newstate);
    }

    public void SetHealth(float currentHealth, float maxHealth)
    {
        animator.SetFloat("life", (currentHealth/ maxHealth));
    }

    public void OnDieEventCaller(){ OnDieAnimationEvents.Invoke();}       
}
