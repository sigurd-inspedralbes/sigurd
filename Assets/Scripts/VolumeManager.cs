using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VolumeManager : MonoBehaviour
{
    private void OnEnable()
    {
        LoadVolume();
    }
    public void SetVolume(float newvalue)
    {
        this.GetComponent<AudioSource>().volume = newvalue;
    }
    public void LoadVolume()
    {
        this.GetComponent<AudioSource>().volume = PlayerPrefs.GetFloat("volume", 0.5f);
    }
}
