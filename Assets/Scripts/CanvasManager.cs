using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasManager : MonoBehaviour
{
    [SerializeField] bool Enable1StCanvas;
    [SerializeField] List<GameObject> canvases;
    private void Awake()
    {
        canvases = new List<GameObject>();
        for (int i = 0; i < this.transform.childCount; i++)
            canvases.Add(this.transform.GetChild(i).gameObject) ;

        if(Enable1StCanvas)
        {
            foreach (GameObject canvas in canvases)
                canvas.SetActive(false);
            canvases[0].SetActive(true);
        }
    }
    public void ActivateCanvas(string canvasname)
    {
        foreach (GameObject canvas in canvases)
        {
            if (canvas.name == canvasname)
                canvas.SetActive(true);
            else
                canvas.SetActive(false);  
        }
    }
}
