using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;

public class LevelManager : MonoBehaviour
{
    public static LevelManager instance;
    public bool debug;

    public int coins = 0;
    public TextMeshProUGUI textoMonedas;

    private void Awake()
    {
        instance = this;
    }

    private void Update()
    {
        if (debug)
        {
            Debug.Log("Coins: " + coins );
        }
    }

    public void IncrementCoin(int valor)
    {
        coins += valor;
        UpdateCoinText();
    }

    private void UpdateCoinText()
    {
        textoMonedas.text = "Monedas: " + coins.ToString();
    }
}
