using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class axe : MonoBehaviour
{
    [SerializeField] GameObject parentenvainado, parentdesenvainado;
    public bool envainada;
    public int damage;
    public UnityEvent OnHit;
    public UnityEvent <bool> OnDesEnvainar;
    
    public void Cambiar()
    {
        envainada = envainada ? false : true;
        if (envainada)
            Envainar();
        else
            Desenvainar();
    }
    public void Envainar()
    {
        envainada = true;
        this.transform.SetParent(parentenvainado.transform);
        ActualizarPosicion();
        OnDesEnvainar.Invoke(this.envainada);
    }
    public void Desenvainar()
    {
        envainada = false;        
        this.transform.SetParent(parentdesenvainado.transform);
        ActualizarPosicion();
        OnDesEnvainar.Invoke(this.envainada);
    }
    private void ActualizarPosicion()
    {
        this.transform.localPosition = Vector3.zero;
        this.transform.localRotation = Quaternion.identity;
        this.transform.localScale = Vector3.one;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<HealthBehaviour>(out HealthBehaviour Hbh))
            Hbh.TakeDamage(this.damage);
        OnHit.Invoke();
    }
}
