using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class LanguageText : MonoBehaviour
{
    public LanguageManager languageManager;
    public string translationKey;

    void Start()
    {
        UpdateText();
        languageManager.languageChangedEvent += UpdateText;
    }

    public void UpdateText()
    {
        if (LanguageManager.instance != null)
        {
            if (translationKey != "")
            {
                string translation = LanguageManager.instance.GetTranslation(translationKey);
                Debug.Log("Traduccion:" + translation);
                if (translation != "")
                {
                    GetComponent<TextMeshProUGUI>().text = translation;
                }
            }
        }
    }
}
