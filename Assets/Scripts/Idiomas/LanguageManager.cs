using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.IO;
public class LanguageManager : MonoBehaviour
{
    public static LanguageManager instance;
    private Dictionary<string, string> translations = new Dictionary<string, string>();
    private string currentLanguage = "english";
    public LanguageText languageText;
    public delegate void LanguageChanged();
    public event LanguageChanged languageChangedEvent;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        LoadLanguage(PlayerPrefs.GetString("lang", "English"));
        languageText = FindObjectOfType<LanguageText>();
    }

    void LoadLanguage(string language)
    {
        translations.Clear();
        string filePath = Path.Combine(Application.streamingAssetsPath, "../Resources/" + language + ".csv");
        Debug.Log("Loading language file from path: " + filePath);

        if (!File.Exists(filePath))
        {
            Debug.LogError("Language file not found at path: " + filePath);
            return;
        }
        using (StreamReader reader = new StreamReader(filePath))
        {
            while (!reader.EndOfStream)
            {
                string line = reader.ReadLine();
                string[] values = line.Split(',');
                if (values.Length == 2)
                {
                    translations.Add(values[0], values[1]);
                }
            }
        }
        if(languageText != null)
            languageChangedEvent.Invoke();
        Debug.Log("Lenguaje cargado: " + language);
    }

    public void SetLanguage(string language)
    {
        currentLanguage = language;
        LoadLanguage(currentLanguage);
        Debug.Log("Lenguaje actual: " + language);
    }
    public string GetTranslation(string key)
    {
        if (translations.ContainsKey(key))
        {
            return translations[key];
        }
        else
        {
            Debug.LogWarning("Translation key not found: " + key);
            return "";
        }
    }
    public void OnLanguageChanged()
    {
        if (LanguageManager.instance != null)
        {
            string language = PlayerPrefs.GetString("lang", "English");
            LanguageManager.instance.SetLanguage(language);
        }
    }
}
