using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistanceEnemy : MonoBehaviour
{
    [SerializeField] public AISimples ai;

    private void Awake()
    {
         ai = gameObject.GetComponent<AISimples>();
        ai.enemyType = 1;
        ai.distanciaAtaque = 5f;
    }

}
