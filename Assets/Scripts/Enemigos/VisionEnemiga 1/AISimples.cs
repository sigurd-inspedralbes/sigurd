using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class AISimples : MonoBehaviour
{

    public Pepe _cabeca;
    NavMeshAgent _navMesh;
    Transform alvo;
    Vector3 posicInicialDaAI;
    Vector3 ultimaPosicConhecida;
    float timerProcura;
    public Animator anim;
    public float distanciaAtaque=1.7f;
    public int enemyType=0;
    public GameObject bulletPrefab;
    public int damage;
    //private HealthBehaviour sigurdHealthBehaviour;
    public bool dmg;
    private bool dead = false;

    [SerializeField] public float timeBetweenArrow;
    private float timeToNextArrow;
    public bool realentizado=false;


    public enum estadoDaAI
    {
        parado, seguindo, procurandoAlvoPerdido, atacando
    };
    public estadoDaAI _estadoAI = estadoDaAI.parado;

    public void SetDeathAnimation()
    {
        dead = true;
        anim.SetBool("Dead", true);
    }

    void Start()
    {
        anim = this.GetComponent<Animator>();
        _navMesh = GetComponent<NavMeshAgent>();
        Debug.Log(_navMesh.ToString());
        alvo = null;
        ultimaPosicConhecida = Vector3.zero;
        _estadoAI = estadoDaAI.parado;
        posicInicialDaAI = transform.position;
        timerProcura = 0;
        timeToNextArrow = 1;
        dmg = true;
    }
    void FixedUpdate()
    {
        timeToNextArrow -= Time.deltaTime;

        if (_cabeca)
        {
            switch (_estadoAI)
            {
                case estadoDaAI.parado:
                    _navMesh.SetDestination(posicInicialDaAI);
                    if (_cabeca.inimigosVisiveis.Count > 0)
                    {
                        alvo = _cabeca.inimigosVisiveis[0];
                        ultimaPosicConhecida = alvo.position;
                        _estadoAI = estadoDaAI.seguindo;
                    }
                    break;
                case estadoDaAI.seguindo:

                    _navMesh.SetDestination(alvo.position);
                    if(Vector3.Distance(gameObject.transform.position, alvo.position) < distanciaAtaque)
                    {
                        _estadoAI = estadoDaAI.atacando;
                        
                    }
                 
                    if (!_cabeca.inimigosVisiveis.Contains(alvo))
                    {
                        ultimaPosicConhecida = alvo.position;
                        _estadoAI = estadoDaAI.procurandoAlvoPerdido;
                    }
                    break;

                case estadoDaAI.atacando:
                    if (Vector3.Distance(gameObject.transform.position, alvo.position) < distanciaAtaque)
                    {
                        if (enemyType == 1)
                        {
                            if (timeToNextArrow <= 0)
                            {
                                timeToNextArrow = timeBetweenArrow;
                                Instantiate(bulletPrefab, this.transform.position + new Vector3(0, .35f, 0), this.transform.rotation);
                            }
                        }
                        anim.SetBool("atk", true);
                        transform.LookAt(alvo.position);
                        
                    }
                    
                    else
                    {
                        anim.SetBool("atk", false);
                        StopCoroutine(meleeDamage());
                        _estadoAI = estadoDaAI.seguindo;
                    }
                    
                    break;
                case estadoDaAI.procurandoAlvoPerdido:
                    _navMesh.SetDestination(ultimaPosicConhecida);
                    timerProcura += Time.deltaTime;
                    if (timerProcura > 5)
                    {
                        timerProcura = 0;
                        _estadoAI = estadoDaAI.parado;
                        break;
                    }
                    if (_cabeca.inimigosVisiveis.Count > 0)
                    {
                        alvo = _cabeca.inimigosVisiveis[0];
                        ultimaPosicConhecida = alvo.position;
                        _estadoAI = estadoDaAI.seguindo;
                    }
                    break;
            }
        }
        if (realentizado)
            _navMesh.speed = 1.0f;
        else
            _navMesh.speed = 3.5f;
        anim.SetFloat("Speed",_navMesh.velocity.magnitude);
    }

    IEnumerator meleeDamage()
    {
        dmg = false;
        yield return new WaitForSeconds(1.5f);
        dmg = true;
    }
    private void OnTriggerStay(Collider other)
    {
        //Debug.LogError(other.gameObject.name);
        if (enemyType == 0 && _estadoAI == estadoDaAI.atacando)
        { 
            anim.SetBool("atk", dmg);
            if (dmg && !dead)
            {
                StartCoroutine(meleeDamage());
                if(other.TryGetComponent<HealthBehaviour>(out HealthBehaviour SigurdHealthBehabiour))
                    SigurdHealthBehabiour.TakeDamage(this.damage);
            }
            else
                StopCoroutine(meleeDamage());
        }
    }
}
