using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pepe : MonoBehaviour
{
    [Header("Geral")]
    public Transform cabecaEnemigo;
    //public bool Raycast = true;
    public enum TipoDeColision
    {
        Raycast, ConoColider
    }

    public TipoDeColision _tipoDeColision = TipoDeColision.ConoColider;

    public enum TipoDeChequeo
    {
        _10PorSegundo, _20PorSegundo, TodoElTiempo
    };
    public TipoDeChequeo _tipoDeChequeo = TipoDeChequeo.TodoElTiempo;
    [Range(1, 50)]
    public float distanciaDeVision = 10;

    [Header("OverlapSphere")]
    public LayerMask layerDeEnemigos = 2;
    public bool desenharEsfera = true;

    [Header("Raycast")]
    public string tagDelPlayer = "Player";
    [Range(2, 180)]
    public float raiosExtraPorCamada = 40;
    [Range(5, 200)]
    public float anguloDeVisao = 200;
    [Range(1, 10)]
    public int numeroDeCamadas = 8;
    [Range(0.02f, 0.15f)]
    public float distanciaEntreCamadas = 0.1f;
    //
    [Space(10)]
    public List<Transform> inimigosVisiveis = new List<Transform>();
    List<Transform> listaTemporariaDeColisoes = new List<Transform>();
    LayerMask layerObstaculos;
    float timerChecagem = 0;

    private void Start()
    {
        timerChecagem = 0;
        if (!cabecaEnemigo)
        {
            cabecaEnemigo = transform;
        }
        // o operador ~ inverte o estado dos bits (0 passa a ser 1, e vice versa)
        layerObstaculos = ~layerDeEnemigos;
    }

    void Update()
    {
        if (_tipoDeChequeo == TipoDeChequeo._10PorSegundo)
        {
            timerChecagem += Time.deltaTime;
            if (timerChecagem >= 0.1f)
            {
                timerChecagem = 0;
                ChecarInimigos();
            }
        }
        if (_tipoDeChequeo == TipoDeChequeo._20PorSegundo)
        {
            timerChecagem += Time.deltaTime;
            if (timerChecagem >= 0.05f)
            {
                timerChecagem = 0;
                ChecarInimigos();
            }
        }
        if (_tipoDeChequeo == TipoDeChequeo.TodoElTiempo)
        {
            ChecarInimigos();
        }
    }

    private void ChecarInimigos()
    {
        if (_tipoDeColision == TipoDeColision.Raycast)
        {
            float limiteCamadas = numeroDeCamadas * 0.5f;
            for (int x = 0; x <= raiosExtraPorCamada; x++)
            {
                for (float y = -limiteCamadas + 0.5f; y <= limiteCamadas; y++)
                {
                    float angleToRay = x * (anguloDeVisao / raiosExtraPorCamada) + ((180.0f - anguloDeVisao) * 0.5f);
                    Vector3 directionMultipl = (-cabecaEnemigo.right) + (cabecaEnemigo.up * y * distanciaEntreCamadas);
                    Vector3 rayDirection = Quaternion.AngleAxis(angleToRay, cabecaEnemigo.up) * directionMultipl;
                    //
                    RaycastHit hitRaycast;
                    if (Physics.Raycast(cabecaEnemigo.position, rayDirection, out hitRaycast, distanciaDeVision))
                    {
                        if (!hitRaycast.transform.IsChildOf(transform.root) && !hitRaycast.collider.isTrigger)
                        {
                            if (hitRaycast.collider.gameObject.CompareTag(tagDelPlayer))
                            {
                                Debug.DrawLine(cabecaEnemigo.position, hitRaycast.point, Color.red);
                                //
                                if (!listaTemporariaDeColisoes.Contains(hitRaycast.transform))
                                    listaTemporariaDeColisoes.Add(hitRaycast.transform);

                                if (!inimigosVisiveis.Contains(hitRaycast.transform))
                                    inimigosVisiveis.Add(hitRaycast.transform);

                            }
                            /*if (hitRaycast.collider.gameObject.CompareTag("Amigos"))
                            {
                                Debug.DrawLine(cabecaEnemigo.position, hitRaycast.point, Color.blue);

                            }*/
                        }
                    }

                    else
                    {
                        Debug.DrawRay(cabecaEnemigo.position, rayDirection * distanciaDeVision, Color.green);
                    }
                }
            }
        }

        if (_tipoDeColision == TipoDeColision.ConoColider)
        {
            Collider[] alvosNoRaioDeAlcance = Physics.OverlapSphere(cabecaEnemigo.position, distanciaDeVision, layerDeEnemigos);
            foreach (Collider targetCollider in alvosNoRaioDeAlcance)
            {
                Transform alvo = targetCollider.transform;
                Vector3 direcaoDoAlvo = (alvo.position - cabecaEnemigo.position).normalized;
                if (Vector3.Angle(cabecaEnemigo.forward, direcaoDoAlvo) < (anguloDeVisao / 2.0f))
                {
                    float distanciaDoAlvo = Vector3.Distance(transform.position, alvo.position);
                    if (!Physics.Raycast(cabecaEnemigo.position, direcaoDoAlvo, distanciaDoAlvo, layerObstaculos))
                    {
                        if (!alvo.transform.IsChildOf(cabecaEnemigo.root))
                        {
                            if (!listaTemporariaDeColisoes.Contains(alvo))
                            {
                                listaTemporariaDeColisoes.Add(alvo);
                            }
                            if (!inimigosVisiveis.Contains(alvo))
                            {
                                inimigosVisiveis.Add(alvo);
                            }
                        }
                    }
                }
            }
            for (int x = 0; x < inimigosVisiveis.Count; x++)
            {
                Debug.DrawLine(cabecaEnemigo.position, inimigosVisiveis[x].position, Color.blue);
            }
        }

        //remove da lista inimigos que n�o est�o visiveis
        for (int x = 0; x < inimigosVisiveis.Count; x++)
        {
            if (!listaTemporariaDeColisoes.Contains(inimigosVisiveis[x]))
            {
                inimigosVisiveis.Remove(inimigosVisiveis[x]);
            }
        }
        listaTemporariaDeColisoes.Clear();
    }

#if UNITY_EDITOR
    private void OnDrawGizmosSelected()
    {
        if (_tipoDeColision == TipoDeColision.ConoColider)
        {
            if (desenharEsfera)
            {
                Gizmos.color = Color.white;
                Gizmos.DrawWireSphere(cabecaEnemigo.position, distanciaDeVision);
            }
            Gizmos.color = Color.green;
            float angleToRay1 = (180.0f - anguloDeVisao) * 0.5f;
            float angleToRay2 = anguloDeVisao + (180.0f - anguloDeVisao) * 0.5f;
            Vector3 rayDirection1 = Quaternion.AngleAxis(angleToRay1, cabecaEnemigo.up) * (-transform.right);
            Vector3 rayDirection2 = Quaternion.AngleAxis(angleToRay2, cabecaEnemigo.up) * (-transform.right);
            Gizmos.DrawRay(cabecaEnemigo.position, rayDirection1 * distanciaDeVision);
            Gizmos.DrawRay(cabecaEnemigo.position, rayDirection2 * distanciaDeVision);
            //
            UnityEditor.Handles.color = Color.green;
            float angle = Vector3.Angle(transform.forward, rayDirection1);
            Vector3 pos = cabecaEnemigo.position + (cabecaEnemigo.forward * distanciaDeVision * Mathf.Cos(angle * Mathf.Deg2Rad));
            UnityEditor.Handles.DrawWireDisc(pos, cabecaEnemigo.transform.forward, distanciaDeVision * Mathf.Sin(angle * Mathf.Deg2Rad));
        }
    }
#endif

   /* public void OnTriggerEnter(Collider other)
    {
        if (!inimigosVisiveis.Contains(other.gameObject.transform) && other.gameObject.CompareTag(tagDelPlayer))
        {
            listaTemporariaDeColisoes.Add(other.gameObject.transform);
            inimigosVisiveis.Add(other.gameObject.transform);
        }

    }

    private void OnTriggerStay(Collider other)
    {
        if (!inimigosVisiveis.Contains(other.gameObject.transform) && other.gameObject.CompareTag(tagDelPlayer))
        {
            listaTemporariaDeColisoes.Add(other.gameObject.transform);

            inimigosVisiveis.Add(other.gameObject.transform);
        }
    }*/
}