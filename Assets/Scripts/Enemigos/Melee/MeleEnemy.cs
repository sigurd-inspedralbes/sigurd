using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleEnemy : MonoBehaviour
{
    [SerializeField] public AISimples ai;

    private void Awake()
    {
        ai = gameObject.AddComponent<AISimples>();
        ai.enemyType = 2;
        ai.distanciaAtaque = 1.7f;
    }

}
