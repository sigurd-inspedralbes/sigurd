using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CreateAssetMenu( fileName = "Hechizos", menuName = "Hechizos", order = 1)]
public class Hechizo : ScriptableObject
{
   
    public GameObject prefabHechizo;
    public int costeMana;
    public Sprite icon;
}
