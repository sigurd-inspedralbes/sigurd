using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ThrowSpell : MonoBehaviour
{
    [SerializeField] public int maxmana;
    [SerializeField]int currentmana;
    public float ManaRecoverySpeed;
    public UnityEvent OnSpellThrowed;
    public List<Hechizo> hechizos;
    public int selectedspell = 0;
    private void Awake()
    {
        hechizos = new List<Hechizo>();
    }
    public void ThrowFireball()
    {
        if (hechizos.Count != 0)
        {
            GameObject aux = Instantiate(hechizos[selectedspell].prefabHechizo, this.transform.position + (new Vector3(0, 1, 0)+transform.forward), this.transform.rotation);
            OnSpellThrowed.Invoke();
        }
    }
    public void nextspell()
    {
        selectedspell++;
        if (selectedspell > hechizos.Count-1)
            selectedspell = 0;
    }
    public Hechizo GetSpell() { return hechizos[selectedspell]; }
}
