using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasRotation : MonoBehaviour
{
    private GameObject cam;

    private void Start()
    {
        cam = GameObject.FindGameObjectWithTag("MainCamera");
    }

    private void Update()
    {
        this.transform.rotation = Quaternion.LookRotation(cam.transform.position - transform.position);
    }
}
