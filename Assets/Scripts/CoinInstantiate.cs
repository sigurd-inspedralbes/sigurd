using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinInstantiate : MonoBehaviour
{
    public GameObject coinPrefab;

    public void InstantiateCoin()
    {
        Instantiate(coinPrefab, transform.position, Quaternion.Euler(90,0,0));
        Destroy(gameObject);
    }
}
