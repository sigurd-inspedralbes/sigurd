using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemiesController : MonoBehaviour
{
    [SerializeField] bool _RestoreEnemiesHealth,_RestoreEnemiesInitialPositition;
    private List<Vector3> enemiespos;
    private List<GameObject> enemies;

    public void RestoreEnemiesHealth()
    {
        if (_RestoreEnemiesHealth)
            for (int i = 0; i < this.transform.childCount; i++)
                this.transform.GetChild(i).GetComponent<HealthBehaviour>().Heal();
    }
    public void RestoreEnemiesInitialPosition()
    {
        for (int i = 0; i < enemies.Count; i++)
        {
            if (enemies[i] != null)
            {
                if (enemies[i].TryGetComponent<CharacterController>(out CharacterController ecc))
                    ecc.enabled = false;
                enemies[i].transform.position = enemiespos[i];
                if (ecc)
                    ecc.enabled = true;
            }
        }
    }

    public void RestoreAll()
    {
        if(_RestoreEnemiesHealth)
            RestoreEnemiesHealth();
        if(_RestoreEnemiesInitialPositition)
            RestoreEnemiesInitialPosition();
    }
    
    private void Awake()
    {
        enemies = new List<GameObject>();
        enemiespos = new List<Vector3>();
        if(_RestoreEnemiesInitialPositition)
        {
            for (int i = 0; i < this.transform.childCount; i++)
            {
                enemies.Add(this.transform.GetChild(i).gameObject);
                enemiespos.Add(this.transform.GetChild(i).transform.position);
            }
        }
    }
}
