using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class TrampaMina : MonoBehaviour
{
    public GameObject explosion;
    public float timer=2.5f;
    public int selectTrampa = 0;
    public UnityEvent OnEnter;


    private void OnTriggerEnter(Collider other)
    {
        
        if (selectTrampa == 0)
            StartCoroutine(detonate());

        if (selectTrampa == 1)
        {
            OnEnter.Invoke();
            Destroy(gameObject);
        }
    }

    IEnumerator detonate() {
        yield return new WaitForSeconds(timer);
            Instantiate(explosion, transform.position, transform.rotation);
        Destroy(gameObject);
    }
}
