using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrampaDardos : MonoBehaviour
{
    // Start is called before the first frame update
    public float timeToNextArrow;
    public float timeBetweenArrow;
    public GameObject bulletPrefab;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        timeToNextArrow -= Time.deltaTime;
        if (timeToNextArrow <= 0)
        {
            timeToNextArrow = timeBetweenArrow;
            Instantiate(bulletPrefab, (this.transform.position + (this.transform.forward*1.5f)), this.transform.rotation);
        }
    }
}
