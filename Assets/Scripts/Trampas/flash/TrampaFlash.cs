using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TrampaFlash : MonoBehaviour
{
    public Image flashEffect;
    public bool playerFlash;
    private float r, g, b, a;
    
    void Start()
    {
        r = flashEffect.color.r;
        g = flashEffect.color.g;
        b = flashEffect.color.b;
        a = flashEffect.color.a;
    }

    public void trampaActivada()
    {
        playerFlash = true;
    }

    void Update()
    {
        if (playerFlash)
        {
            a += 0.7f;
            playerFlash = false;
        }
        else
        {
            a -= 0.001f;
        }

        a = Mathf.Clamp(a, 0, 1f);

        ChangeColor();
    }

    private void ChangeColor()
    {
        Color c = new Color(r, g, b, a);
        flashEffect.color = c;
    }
}
