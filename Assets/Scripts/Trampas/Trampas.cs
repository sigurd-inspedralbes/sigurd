using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CreateAssetMenu(fileName = "Trampas", menuName = "Trampas", order = 1)]
public class Trampas : ScriptableObject
{
    public GameObject prefabTrampa;
}
