using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForwardRepulsor : MonoBehaviour
{
    [SerializeField]float power;
    private void OnTriggerStay(Collider other)
    {
        Vector3 dir = this.transform.forward;
        other.GetComponent<CharacterController>().Move(dir * power);
    }
}
