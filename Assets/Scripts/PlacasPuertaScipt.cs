using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlacasPuertaScipt : MonoBehaviour
{
    public bool presionada=false, canundo;
    private void OnTriggerEnter(Collider other)
    {
        this.GetComponentInParent<MecanismoPlacas>().IncrementPressed();
        presionada = true;
    }
    private void OnTriggerExit(Collider other)
    {
        if (canundo) 
        {
            this.GetComponentInParent<MecanismoPlacas>().DecrementPressed();
            presionada = false; 
        }
            
    }
}
