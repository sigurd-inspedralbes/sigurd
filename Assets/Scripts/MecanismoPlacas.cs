using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MecanismoPlacas : MonoBehaviour
{
    public int placasAct;
    public UnityEvent Reward,UndoReward;
    [SerializeField] bool canredoreward; 
    public void IncrementPressed()
    {
        placasAct++;
        if (placasAct == this.transform.childCount)
            Reward.Invoke();   
    }
    public void DecrementPressed()
    {
        if (placasAct == this.transform.childCount && canredoreward)
            UndoReward.Invoke();
        placasAct--;
    }
}
