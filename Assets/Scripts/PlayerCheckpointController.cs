using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCheckpointController : MonoBehaviour
{
    public Vector3 checkpointpos;
    void Start()
    {
        SetRespawnPos();
    }
    
    public void SetRespawnPos()
    {
        this.checkpointpos = this.transform.position;
    }
    
    public void Respawn()
    {
        if(this.TryGetComponent<CharacterController>(out CharacterController cc))
        {
            cc.enabled = false;
            this.transform.position = this.checkpointpos;
            cc.enabled = true;
        }
        else
            this.transform.position = this.checkpointpos; 
        this.GetComponent<HealthBehaviour>().Heal();
    }
}
