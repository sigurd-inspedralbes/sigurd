using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Diana : MonoBehaviour
{
    public UnityEvent OnHitted;
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.TryGetComponent<BulletSpell>(out BulletSpell b))
        {
            OnHitted.Invoke();
            transform.parent.GetComponentInParent<MecanismoDianas>().CheckForChildCount();
            Destroy(this.transform.parent.gameObject);
        }

    }
}
