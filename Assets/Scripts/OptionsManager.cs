using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class OptionsManager : MonoBehaviour
{
    
    [SerializeField] float volume;
    [SerializeField] Slider volumeslider;

    [SerializeField] bool fullscreen;
    [SerializeField] Toggle fullscreentoggle;

    [SerializeField] string lang;
    [SerializeField] TMP_Dropdown langdropdown;



    public void SaveOptions()
    {
        volume = volumeslider.value;
        fullscreen = fullscreentoggle.isOn;
        lang = langdropdown.options[langdropdown.value].text;

        PlayerPrefs.SetFloat("volume", volume);
        PlayerPrefs.SetInt("fullscreen", fullscreen ? 1 : 0);
        PlayerPrefs.SetString("lang", lang);
        PlayerPrefs.Save();

        Screen.fullScreen = this.fullscreen;
    }

    private void OnEnable()
    {
        LoadOptions();
    }

    public void LoadOptions()
    {
        this.volume = PlayerPrefs.GetFloat("volume", 50);
        volumeslider.value = this.volume;

        if (PlayerPrefs.GetInt("fullscreen", 1) == 0)
            fullscreen = false;
        else
            fullscreen = true;
        fullscreentoggle.isOn = this.fullscreen;
        Screen.fullScreen = this.fullscreen;

        lang = PlayerPrefs.GetString("lang", "English");
        for (int i = 0; i < langdropdown.options.Count; i++)
            if (langdropdown.options[i].text == lang)
                langdropdown.value = i;

    }
    

}
