using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class HealthBehaviour : MonoBehaviour
{
    public float maxHealth;
    public float currentHealth;
    public UnityEvent<float, float> hurtEvent;
    public UnityEvent OnDie;

    void Start()
    {
        currentHealth = maxHealth;
        //hurtEvent.Invoke(currentHealth, maxHealth);
    }
    private void Update()
    {
        hurtEvent.Invoke(currentHealth, maxHealth);
    }
    public void TakeDamage(float damageValue)
    {
        currentHealth -= damageValue;
        hurtEvent.Invoke(currentHealth, maxHealth);
        if (currentHealth <= 0)
            OnDie.Invoke();
    }
    public void TakeDamage()
    {
        currentHealth--;
        hurtEvent.Invoke(currentHealth, maxHealth);
        if (currentHealth <= 0)
            OnDie.Invoke();
    }
    public void AddHealth(float healingValue)
    {
        currentHealth += healingValue;
        if (currentHealth > maxHealth)
            currentHealth = maxHealth;
        hurtEvent.Invoke(currentHealth, maxHealth);
    }
    public void AddHealth()
    {
        currentHealth++;
        if (currentHealth > maxHealth)
            currentHealth = maxHealth;
        hurtEvent.Invoke(currentHealth, maxHealth);
    }
    public void Heal() { this.currentHealth = maxHealth; }
    
}
