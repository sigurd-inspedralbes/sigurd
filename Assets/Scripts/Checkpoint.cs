using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    [SerializeField] bool RestoreHealthOnTouch;
    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<PlayerCheckpointController>(out PlayerCheckpointController psc))
        {
            psc.SetRespawnPos();
            if (RestoreHealthOnTouch)
                other.GetComponent<HealthBehaviour>().Heal();
        }
    }
}
