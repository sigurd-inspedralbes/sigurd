using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour
{
    public int rutina;
    public float cronometro;
    public Animator ani;
    public Quaternion angulo;
    public float grado;
    public bool attack;
    public float walkN;
    public float speed = 1;
    private CharacterController enemyController;

    public GameObject target;

    void Start()
    {
        ani = GetComponent<Animator>();
        target = GameObject.Find("PlayerArmature");
        enemyController = GetComponent<CharacterController>();
    }

    public void Comportamiento_Enemigo()
    {
        if (Vector3.Distance(transform.position, target.transform.position) > 7)
        {

            cronometro += 1 * Time.deltaTime;
            if (cronometro >= 4)
            {
                rutina = Random.Range(0, 2);
                cronometro = 0;
            }
            switch (rutina)
            {
                case 0:
                    walkN = 0f;
                    ani.SetFloat("Speed", walkN);
                    break;

                case 1:
                    grado = Random.Range(0, 360);
                    angulo = Quaternion.Euler(0, grado, 0);
                    rutina++;
                    break;
                case 2:
                   Vector3 giro = Quaternion.RotateTowards(transform.rotation, angulo, 0.5f)* new Vector3(0f,0f,0f);
                    if (walkN >= 0.5)
                        walkN -= Time.deltaTime;
                    else
                        walkN += Time.deltaTime;
                   // enemyController.Move(giro.normalized * Time.deltaTime + new Vector3(1f, 0f, 0f));

                    //transform.Translate(Vector3.forward * walkN * Time.deltaTime);
                    ani.SetFloat("Speed", walkN);
                    break;
            }
        }
        else
        {

            if (Vector3.Distance(transform.position,target.transform.position)>1&&!attack)
            {
                var lookPos = target.transform.position - transform.position;
                lookPos.y = 0;
                var rotation = Quaternion.LookRotation(lookPos);
                transform.rotation = Quaternion.RotateTowards(transform.rotation, rotation, 2);
                //walkN = 3f;
                if (walkN >= 3)
                    walkN -= Time.deltaTime;
                else
                    walkN += Time.deltaTime;
                //transform.Translate(Vector3.forward * walkN * Time.deltaTime);
                
                enemyController.Move(new Vector3(0f,0f,0f) * walkN * Time.deltaTime);
                ani.SetFloat("Speed", walkN);
            }
            else
            {
                //ani.SetBool("Atk",true);
                
                //walkN = 1;
                if (walkN >= 1)
                    walkN -= Time.deltaTime;
                else
                    walkN += Time.deltaTime;
                ani.SetFloat("Attack", walkN);
                    
            }
            
        }
        
    }

    private void Update()
    {
        Comportamiento_Enemigo();
    }

}
