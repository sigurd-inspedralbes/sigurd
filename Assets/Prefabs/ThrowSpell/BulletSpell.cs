using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletSpell : MonoBehaviour
{
    public float spellSpeed;
    public int damage;
    private Rigidbody rgb;
    private HealthBehaviour hbr;
    //public HealthBehabiour damage;

    void Start()
    {
        rgb = GetComponent<Rigidbody>();
        hbr = GetComponent<HealthBehaviour>();
    }

    // Update is called once per frame
    void Update()
    {
        rgb.velocity = new Vector3(transform.forward.x, 0, transform.forward.z) * spellSpeed;
    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.TryGetComponent<HealthBehaviour>(out HealthBehaviour hbr))
            hbr.TakeDamage(this.damage);
        
        Destroy(this.gameObject);
    }
}
